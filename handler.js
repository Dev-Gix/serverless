const SchemaTransaction = require('./schema');

module.exports.main = (event, context, callback) => {
  const {value,error } = SchemaTransaction.validate(JSON.parse(event.body).transactions);
  
  if (error) {
    const response = {
      statusCode: 404,
      body: error.message
    };
    callback(null, response);
  } else {
    try {
      const transactionARG = filterCurrency(value, "ARG");
      const transactionUSD = filterCurrency(value, "USD");

      const result = {
        "balance": {
          "amounts": {
            "ars": transactionARG.reduce((t, { amount }) => t + amount, 0),
            "usd": transactionUSD.reduce((t, { amount }) => t + amount, 0)
          }
        }
      }
      const response = {
        statusCode: 200,
        body: JSON.stringify(result),
      };
      callback(null, response);
    } catch (e) {
      console.log(`error: ${e}`)
    }
  }

};

/**
 * filter successful transactions
 * @param {Array} transactions
 * @param {string} currency
 * @returns {Array}
 */
function filterCurrency(transactions, currency) {
  return transactions.filter((t) => t.currency === currency && t.status === "succeeded");
}

