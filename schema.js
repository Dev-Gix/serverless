const Joi = require("joi");

const SchemaTransaction = Joi.array().items(
    Joi.object({
      amount: Joi.number().required(),
      currency: Joi.string().min(3).max(10).required(),
      vendor: Joi.string().required(),
      status: Joi.string().required(),
    })
  );


module.exports = SchemaTransaction;
